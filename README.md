A script for updating certificates in a Sophos UTM firewall
===========================================================

This is a script that can be used to update existing certificate
objects in a Sophos UTM firewall with new ones. A blog post describing
how to use this will be available on my
[company's web site](https://www.linet-services.de/) (in German).

It can be used for e.g. interfacing with a Let's Encrypt client.

Contact
=======

Written by Moritz Bunkus <m.bunkus@linet-services.de>.

License
=======

This software is licensed under the terms of the [MIT license](LICENSE.md).
